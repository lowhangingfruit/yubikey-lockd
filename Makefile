CC = clang

FRAMEWORKS := -framework IOKit -framework CoreFoundation

SOURCE = yubikey-lockd.c

CFLAGS = -Wall -Werror -O2 $(SOURCE)
LDFLAGS = $(LIBRARIES) $(FRAMEWORKS)
OUT = -o yubikey-lockd

all: yubikey-lockd

clean:
	rm -rf yubikey-lockd

yubikey-lockd:
	$(CC) $(CFLAGS) $(LDFLAGS) $(OUT)
