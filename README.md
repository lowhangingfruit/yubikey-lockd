# yubikey-lockd

The application (should) run as a deamon on macOS. It monitors for YubiKey devices. On removal of the YubiKey the screen will be locked.

Based on Serg Podtynnyi's [yubikeylockd](https://github.com/shtirlic/yubikeylockd).

Main reason to rewrite Serg's version was that our company security profile does not allow us to change the "Require password after sleep" timeout. It's value is 15 minutes by default. So it would be just a screensaver and no workstation lock.

Major differences:

- Locking the screen is now based on fast-user switching ("CGSession - suspend").
  The previous solution depended on the security option "Require password x minutes after sleep or screen er begins".
- Apple's Unified logging implemented additionally to the console logging.
  For more information read Apple's [Logging](https://developer.apple.com/documentation/os/logging) documentation.
- Removed the optional commandline parameter to override the USB vendor ID.


## Build from source

```
make - f Makefile
```

or just `make`.

## Installation instructions

The instalation setup is still work in progress.

```
sudo brew services start yubikey-lockd
```

## Todo:

Stuff to do.

### Make a brew installation package.

```
brew install yubikey-lockd
```
