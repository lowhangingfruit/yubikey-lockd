// lockscreen.c
//
// Activates the screensaver immediately
// but with some side effects ...
//
// This is just a piece of sample code to envoke the screensaver really fast
// but it is still just the screensaver that kicks in.
//
// Build with:
// clang -F /System/Library/PrivateFrameworks -framework login -o lockscreen lockscreen.c
//
extern int SACLockScreenImmediate ( void );

int main ( ) {
    return SACLockScreenImmediate();
}
